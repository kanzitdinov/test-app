//
//  UserDefaults.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 22.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit

class UserDefaults: NSObject {
    
    // MARK: Variables
    
    static let selectedCity = "SelectedCity"
    static let changed = "changed"
    static let favouritesCountries = "favouritesCountries"
    static let defaults = NSUserDefaults.standardUserDefaults()
    
    // MARK: Functions
    
    static func setSelectedCity(city: City) {
        let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(city)
        defaults.setObject(data, forKey: selectedCity)
    }
    
    static func getSelectedCity() -> City? {
        guard let data = defaults.objectForKey(selectedCity)
            else {
                return nil
        }
        
        return NSKeyedUnarchiver.unarchiveObjectWithData(data as! NSData) as? City
    }
    
    // MARK: If changed

    static func setIfChagned(was: Bool) {
        defaults.setBool(was, forKey: changed)
    }
    
    static func getIfChagned() -> Bool {
        return defaults.boolForKey(changed)
    }
    
    // MARK: Favourites Countries manipulations
    
    static func addCountryToFavourites(country: Country) {
        
        guard let data = defaults.objectForKey(favouritesCountries)
            else {
                print("ADD ELSE")
                var countriesSet = Set<Country>()
                countriesSet.insert(country)
                let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(countriesSet)
                defaults.setObject(data, forKey: favouritesCountries)
                
                return
        }
        
        print("ADD NORM \(country.name) ++ \(country.id)")
        
        var countriesSet = NSKeyedUnarchiver.unarchiveObjectWithData(data as! NSData) as! Set<Country>
        countriesSet.insert(country)
        
        let extraData: NSData = NSKeyedArchiver.archivedDataWithRootObject(countriesSet)
        
        defaults.setObject(extraData, forKey: favouritesCountries)
        
    }
    
    static func removeCountryFromFavourites(country: Country) {
        
        guard let data = defaults.objectForKey(favouritesCountries)
            else {
                print("REMOVE ELSE")
                return
        }
        
        print("REMOVE NORM \(country.name) ++ \(country.id)")
        
        var countriesSet = NSKeyedUnarchiver.unarchiveObjectWithData(data as! NSData) as! Set<Country>
        countriesSet.remove(country)
        
        let extraData: NSData = NSKeyedArchiver.archivedDataWithRootObject(countriesSet)
        
        defaults.setObject(extraData, forKey: favouritesCountries)
        
    }
    
    static func getSetOfFavouritesCountries() -> Set<Country> {
        
        guard let data = defaults.objectForKey(favouritesCountries)
            else {
                return Set<Country>()
        }
        
        return NSKeyedUnarchiver.unarchiveObjectWithData(data as! NSData) as! Set<Country>
                
    }

}












