//
//  SettingsViewController.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 22.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    // MARK: App cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setting variables
        initSettings()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Init methods
    func initSettings() {
        self.title = "Настройки"
    }
    
}
