//
//  Help.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 23.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit

class Help: NSObject {

    static let GET_COUNTRIES_LINK = "https://atw-backend.azurewebsites.net/api/countries"
    
    static func defaultOnePixel() -> CGFloat {
        return 1.0/UIScreen.mainScreen().scale
    }
    
}
