//
//  Image.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 23.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit
import SwiftyJSON

class Image: NSObject {

    var imageLink: NSURL!
    var shouldShowWatermark: Bool!
    
    init(imageResp: JSON) {
        
        super.init()
        
        self.imageLink = NSURL(string: imageResp["ImageLink"].stringValue)
        self.shouldShowWatermark = imageResp["ShouldShowWatermark"].boolValue
        
    }
    
    init(imageLink: NSURL, shouldShowWatermark: Bool) {
        
        super.init()
        
        self.imageLink = imageLink
        self.shouldShowWatermark = shouldShowWatermark
        
    }
    
    // Hashable and Equitable
    
    override var hash: Int {
        return imageLink.hashValue
    }
    
    /*
    override func isEqual(object: AnyObject?) -> Bool {
        guard let rhs = object as? Image else {
            return false
        }
        let lhs = self
        
        return lhs.imageLink == rhs.imageLink
    }
 */
    
    // MARK: NSCoding
    
    required convenience init?(coder decoder: NSCoder) {
            guard let imageLink = decoder.decodeObjectForKey("imageLink") as? NSURL,
                let shouldShowWatermark = decoder.decodeObjectForKey("shouldShowWatermark") as? Bool
            else { return nil }
        
        self.init(imageLink: imageLink, shouldShowWatermark: shouldShowWatermark)
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.imageLink, forKey: "imageLink")
        coder.encodeObject(self.shouldShowWatermark, forKey: "shouldShowWatermark")
    }
    
}










