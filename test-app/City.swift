//
//  City.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 23.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit
import SwiftyJSON

class City: NSObject {

    var id: Int!
    var countryId: Int!
    var name: String!
    var imageLink: NSURL!
    var artists: [Artist]!
    
    init(cityResp: JSON) {
        
        super.init()
        
        self.id = cityResp["Id"].intValue
        self.countryId = cityResp["CountryId"].intValue
        self.name = cityResp["Name"].stringValue
        self.imageLink = NSURL(string: cityResp["ImageLink"].stringValue)
        self.artists = getArtistsFrom(artistsArray: cityResp["Artists"].arrayValue)
        
    }
    
    init(id: Int, countryId: Int, name: String, imageLink: NSURL, artists: [Artist]) {
        
        super.init()
        
        self.id = id
        self.countryId = countryId
        self.name = name
        self.imageLink = imageLink
        self.artists = artists
        
    }
    
    func getArtistsFrom(artistsArray artistsArray: [JSON]) -> [Artist] {
        var tmpArtists = [Artist]()
        
        for artistInfo in artistsArray {
            tmpArtists.append(Artist(artistResp: artistInfo))
        }
        
        return tmpArtists
    }
    
    // Hashable and Equitable
    
    override var hash: Int {
        return id.hashValue
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        guard let rhs = object as? City else {
            return false
        }
        let lhs = self
        
        return lhs.id == rhs.id
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder decoder: NSCoder) {
        guard let id = decoder.decodeObjectForKey("id") as? Int,
            let countryId = decoder.decodeObjectForKey("countryId") as? Int,
            let name = decoder.decodeObjectForKey("name") as? String,
            let imageLink = decoder.decodeObjectForKey("imageLink") as? NSURL,
            let artists = decoder.decodeObjectForKey("artists")
            else { return nil }
        
        self.init(id: id, countryId: countryId, name: name, imageLink: imageLink, artists: artists as! [Artist])
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.id, forKey: "id")
        coder.encodeObject(self.countryId, forKey: "countryId")
        coder.encodeObject(self.name, forKey: "name")
        coder.encodeObject(self.imageLink, forKey: "imageLink")
        coder.encodeObject(self.artists, forKey: "artists")
    }
    
}













