//
//  CountryFirstCell.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 23.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var favouriteButton: UIButton!
    
}
