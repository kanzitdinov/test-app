//
//  Response.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 23.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit
import SwiftyJSON

class Response: NSObject {

    var timestamp: String!
    var error: String!
    var result: [Country]!
    
    init(resp: JSON) {
        
        super.init()
        
        self.timestamp = resp["Timestamp"].stringValue
        self.error = resp["Error"].stringValue
        self.result = getCountriesFrom(countriesArray: resp["Result"].arrayValue)
        
    }
    
    func getCountriesFrom(countriesArray countriesArray: [JSON]) -> [Country]! {
        var tmpCountries = [Country]()
        
        for countryInfo in countriesArray {
            tmpCountries.append(Country(countryResp: countryInfo))
        }
        
        return tmpCountries
    }
    
}
