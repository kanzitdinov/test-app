//
//  CityCell.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 23.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
}
