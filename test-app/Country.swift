//
//  Country.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 23.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit
import SwiftyJSON

class Country: NSObject {

    var id: Int!
    var name: String!
    var imageLink: NSURL!
    var cities: [City]!
    
    init(countryResp: JSON) {
        
        super.init()
        
        self.id = countryResp["Id"].intValue
        self.name = countryResp["Name"].stringValue
        self.imageLink = NSURL(string: countryResp["ImageLink"].stringValue)
        self.cities = getCitiesFrom(citiesArray: countryResp["Cities"].arrayValue)
        
    }
    
    init(id: Int, name: String, imageLink: NSURL, cities: [City]) {
        
        super.init()
        
        self.id = id
        self.name = name
        self.imageLink = imageLink
        self.cities = cities
        
    }
    
    func getCitiesFrom(citiesArray citiesArray: [JSON]) -> [City] {
        var tmpCities = [City]()
        
        for cityInfo in citiesArray {
            tmpCities.append(City(cityResp: cityInfo))
        }
        
        return tmpCities
    }
    
    // Hashable and Equitable
    
    override var hash: Int {
        return id.hashValue
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        guard let rhs = object as? Country else {
            return false
        }
        let lhs = self
        
        return lhs.id == rhs.id
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder decoder: NSCoder) {
        guard let id = decoder.decodeObjectForKey("id") as? Int,
            let name = decoder.decodeObjectForKey("name") as? String,
            let imageLink = decoder.decodeObjectForKey("imageLink") as? NSURL,
            let cities = decoder.decodeObjectForKey("cities")
            else { return nil }
        
        self.init(id: id, name: name, imageLink: imageLink, cities: cities as! [City])
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.id, forKey: "id")
        coder.encodeObject(self.name, forKey: "name")
        coder.encodeObject(self.imageLink, forKey: "imageLink")
        coder.encodeObject(self.cities, forKey: "cities")
    }
    
}
