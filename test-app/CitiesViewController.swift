//
//  CitiesViewController.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 22.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import PKHUD

class CitiesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {

    // MARK: Outlets
    @IBOutlet weak var citiesTableView: UITableView!
    
    var refreshControl: UIRefreshControl!
    
    // MARK: Variables
    var selectedCity: City?
    var wasFirstInit = false
    var wasFirstDownload = false
    var response: Response!
    
    var expandSections: [Bool]!
    
    // MARK: App cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initSettings()
        downloadInfo()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if UserDefaults.getIfChagned() && wasFirstInit {
            
            UserDefaults.setIfChagned(false)
            self.citiesTableView.reloadData()
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Downloading info from server
    func downloadInfo() {
        
        self.pleaseWait()
        
        Alamofire.request(.GET, Help.GET_COUNTRIES_LINK)
            .responseJSON(completionHandler: { response in
        
                if response.result.value != nil {
                    
                    self.response = Response(resp: JSON(response.result.value!))
                    
                    if !self.wasFirstDownload {
                        
                        self.wasFirstDownload = true
                        
                        self.expandSections = [Bool](count:self.response.result.count, repeatedValue: false)
                    }
                    
                    self.citiesTableView.reloadData()
                    
                } else {
                    
                    self.noticeError("")
                    
                }
                
                self.clearAllNotice()
                self.refreshControl.endRefreshing()
        })
        
    }
    
    // MARK: Table View Delegate Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if self.response != nil {
            if self.response.result.count > 0 {
                return self.response.result.count
            }
        }
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.response != nil && self.expandSections[section] {
            return self.response.result[section].cities.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let currentCountry: Country = self.response.result[section]
        
        let countryCellIdentifier = "CountryCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(countryCellIdentifier) as! CountryCell
        
        cell.countryName.text = currentCountry.name
        cell.favouriteButton.tag = currentCountry.id
        
        cell.favouriteButton.addTarget(self, action: #selector(CitiesViewController.favouriteButtonClicked(_:)), forControlEvents: .TouchUpInside)
        
        let countriesSet = UserDefaults.getSetOfFavouritesCountries()
        if countriesSet.contains(currentCountry) {
            cell.favouriteButton.setImage(UIImage(named: "star_filled"), forState: .Normal)
        } else {
            cell.favouriteButton.setImage(UIImage(named: "star"), forState: .Normal)
        }
        
        // нажатие на header
        let oneTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(CitiesViewController.tableViewHeaderTapped(_:)))
        oneTapRecognizer.delegate = self
        cell.contentView.userInteractionEnabled = true
        cell.contentView.addGestureRecognizer(oneTapRecognizer)
        cell.contentView.tag = section
        
        // background
        cell.contentView.backgroundColor = UIColor.whiteColor()
        
        // separator
        let separatorFrame = CGRectMake(8, cell.contentView.frame.size.height - Help.defaultOnePixel(), cell.contentView.frame.size.width, Help.defaultOnePixel())
        let separatorView = UIView(frame: separatorFrame)
        separatorView.backgroundColor = tableView.separatorColor
        cell.contentView.addSubview(separatorView)
        
        return cell.contentView
    }
    
    func tableViewHeaderTapped(recognizer: UITapGestureRecognizer) {
        
        let section = recognizer.view?.tag
        self.expandSections[section!] = !self.expandSections[section!]
        
        self.citiesTableView.reloadSections(NSIndexSet(index: section!), withRowAnimation: .Automatic)
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let currentCity: City = self.response.result[indexPath.section].cities[indexPath.row]
        
        let cityCellIdentifier = "CityCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cityCellIdentifier, forIndexPath: indexPath) as! CityCell
        
        cell.name.text = currentCity.name
        
        if selectedCity != nil && selectedCity?.id == currentCity.id && selectedCity?.countryId == currentCity.countryId {
            
            tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .None)
            
        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let currentCity: City = self.response.result[indexPath.section].cities[indexPath.row]
        
        UserDefaults.setSelectedCity(currentCity)
        selectedCity = UserDefaults.getSelectedCity()
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    // MARK: Some help methods
    func favouriteButtonClicked(sender: UIButton) {
        
        // здесть tag - это айди страны, чтобы легче было искать)
        
        UserDefaults.setIfChagned(true)
        
        var currentCountry: Country!
        for i in 0..<self.response.result.count {
            let tmpCountry: Country = self.response.result[i]
            if tmpCountry.id == sender.tag {
                currentCountry = tmpCountry
                break
            }
        }
        
        // добавить в избранное
        if sender.currentImage == UIImage(named: "star") {
            
            print(currentCountry.name)
            
            sender.setImage(UIImage(named: "star_filled"), forState: .Normal)
            UserDefaults.addCountryToFavourites(currentCountry)
            
        } else { // убрать из избранного
            
            print(currentCountry.name)
            
            sender.setImage(UIImage(named: "star"), forState: .Normal)
            UserDefaults.removeCountryFromFavourites(currentCountry)
            
        }
        
    }
    
    func refresh(sender:AnyObject) {
        downloadInfo()
    }
    
    // MARK: Init methods
    func initSettings() {
        self.title = "Города"
        
        selectedCity = UserDefaults.getSelectedCity()
        
        initTableView()
        
        wasFirstInit = true
    }
    
    func initTableView() {
        self.citiesTableView.delegate = self
        self.citiesTableView.dataSource = self
        
        initPullToRefresh()
    }
    
    func initPullToRefresh() {
        refreshControl = UIRefreshControl()
        // refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(CitiesViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.citiesTableView.addSubview(refreshControl)

    }
}

