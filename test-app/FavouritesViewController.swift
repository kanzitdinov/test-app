//
//  FavouritesViewController.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 22.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit

class FavouritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // MARK: Outlets
    @IBOutlet weak var favouritesTableView: UITableView!
    
    // MARK: Variables
    var favouritesCountries = Array<Country>()
    var wasFirstInit = false
    
    // MARK: App cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setting variables
        initSettings()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if UserDefaults.getIfChagned() && wasFirstInit {
            
            UserDefaults.setIfChagned(false)
            setFavouritesCountries()
            self.favouritesTableView.reloadData()
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Table View Delegate Methods
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouritesCountries.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let currentCountry: Country = favouritesCountries[favouritesCountries.startIndex.advancedBy(indexPath.row)]
        
        let countryCellIdentifier = "CountryCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(countryCellIdentifier, forIndexPath: indexPath) as! CountryCell
        
        cell.countryName.text = currentCountry.name
        cell.favouriteButton.setImage(UIImage(named: "delete"), forState: .Normal)
        cell.favouriteButton.tag = currentCountry.id
        
        cell.favouriteButton.addTarget(self, action: #selector(FavouritesViewController.removeButtonClicked(_:)), forControlEvents: .TouchUpInside)
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // MARK: Some help methods
    func removeButtonClicked(sender: UIButton) {

        // здесть tag - это айди страны, чтобы легче было удалять)
        
        UserDefaults.setIfChagned(true)
        
        for i in 0..<favouritesCountries.count {
            let currentCountry = favouritesCountries[i]
            if currentCountry.id == sender.tag {
                
                UserDefaults.removeCountryFromFavourites(currentCountry)
                favouritesCountries.removeAtIndex(i)
                self.favouritesTableView.deleteRowsAtIndexPaths([ NSIndexPath(forRow: i, inSection: 0) ], withRowAnimation: .Automatic)
                break
                
            }
        }
        
    }
    
    func setFavouritesCountries() {
        favouritesCountries = Array(UserDefaults.getSetOfFavouritesCountries())
    }

    // MARK: Init methods
    func initSettings() {
        self.title = "Избранное"
        
        setFavouritesCountries()
        
        initTableView()
    }
    
    func initTableView() {
        self.favouritesTableView.delegate = self
        self.favouritesTableView.dataSource = self
        
        wasFirstInit = true
    }

}

