//
//  Artist.swift
//  test-app
//
//  Created by Batyr Kanzitdinov on 23.05.16.
//  Copyright © 2016 Batyr Kanzitdinov. All rights reserved.
//

import UIKit
import SwiftyJSON

class Artist: NSObject {

    var id: Int!
    var cityId: Int!
    var age: Int!
    var firstName: String!
    var lastName: String!
    var description_: String!
    var email: String!
    var phone: String!
    var viber: String!
    var images: [Image]!
    
    init(artistResp: JSON) {
        
        super.init()
        
        self.id = artistResp["Id"].intValue
        self.cityId = artistResp["CityId"].intValue
        self.age = artistResp["Age"].intValue
        self.firstName = artistResp["FirstName"].stringValue
        self.lastName = artistResp["LastName"].stringValue
        self.description_ = artistResp["Description"].stringValue
        self.email = artistResp["Email"].stringValue
        self.phone = artistResp["Phone"].stringValue
        self.viber = artistResp["Viber"].stringValue
        self.images = getImagesFrom(imagesArray: artistResp["Images"].arrayValue)
        
    }
    
    init(id: Int, cityId: Int, age: Int, firstName: String, lastName: String, description_: String, email: String, phone: String, viber: String, images: [Image]) {
        
        super.init()
        
        self.id = id
        self.cityId = cityId
        self.age = age
        self.firstName = firstName
        self.lastName = lastName
        self.description_ = description_
        self.email = email
        self.phone = phone
        self.viber = viber
        self.images = images
        
    }
    
    func getImagesFrom(imagesArray imagesArray: [JSON]) -> [Image] {
        var tmpImages = [Image]()
        
        for imgInfo in imagesArray {
            tmpImages.append(Image(imageResp: imgInfo))
        }
        
        return tmpImages
    }
    
    // Hashable and Equitable
    
    override var hash: Int {
        return id.hashValue
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        guard let rhs = object as? Artist else {
            return false
        }
        let lhs = self
        
        return lhs.id == rhs.id
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder decoder: NSCoder) {
        guard let id = decoder.decodeObjectForKey("id") as? Int,
            let cityId = decoder.decodeObjectForKey("cityId") as? Int,
            let age = decoder.decodeObjectForKey("age") as? Int,
            let firstName = decoder.decodeObjectForKey("firstName") as? String,
            let lastName = decoder.decodeObjectForKey("lastName") as? String,
            let description_ = decoder.decodeObjectForKey("description_") as? String,
            let email = decoder.decodeObjectForKey("email") as? String,
            let phone = decoder.decodeObjectForKey("phone") as? String,
            let viber = decoder.decodeObjectForKey("viber") as? String,
            let images = decoder.decodeObjectForKey("images")
            else { return nil }
        
        self.init(id: id, cityId: cityId, age: age, firstName: firstName, lastName: lastName, description_: description_, email: email, phone: phone, viber: viber, images: images as! [Image])
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.id, forKey: "id")
        coder.encodeObject(self.cityId, forKey: "cityId")
        coder.encodeObject(self.age, forKey: "age")
        coder.encodeObject(self.firstName, forKey: "firstName")
        coder.encodeObject(self.lastName, forKey: "lastName")
        coder.encodeObject(self.description_, forKey: "description_")
        coder.encodeObject(self.email, forKey: "email")
        coder.encodeObject(self.phone, forKey: "phone")
        coder.encodeObject(self.viber, forKey: "viber")
        coder.encodeObject(self.images, forKey: "images")
    }
    
}







